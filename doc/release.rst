..
    :copyright: Copyright (c) 2014 ftrack

########
Releases
########

.. release:: 0.1.1
    :date: 2015-05-30

    .. change:: change
        :tags: documentation

        Update documentation to reflect that package is now installable from
        PyPi.

.. release:: 0.1.0
    :date: 2015-01-06

    .. change:: new

        Initial release.

        Includes support for specifying releases with specific changes listed.
        Each change supports tags as well as a main category and changeset link.

